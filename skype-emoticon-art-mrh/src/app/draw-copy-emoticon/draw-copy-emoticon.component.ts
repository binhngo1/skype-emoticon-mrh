import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as EmoticonSelector from '../store/selectors/emoticon.selector';
import * as EmoticonAction from '../store/actions/emoticon.action';
import { Emoticon } from '../store/models/emoticon.model';
import { AppState } from '../store/models/app.state';
import { EmoticonState } from '../store/models/emoticon.state';
import { EmticonDraw } from '../store/models/emticon.draw';

@Component({
    selector: 'draw-copy-emoticon-component',
    templateUrl: './draw-copy-emoticon.component.html',
    styleUrls: ['./draw-copy-emoticon.component.css']
})

export class DrawCopyEmoticonComponent implements OnInit {
    drawingEmoticon$: Observable<EmticonDraw[]>;
    drawingEmoticonArr: EmticonDraw[] = [];

    rows = Array(5).fill(0).map((x,i)=>i);
    cols = Array(5).fill(0).map((x,i)=>i);

    constructor(private store: Store<AppState>) {
        this.drawingEmoticon$ = this.store.select(EmoticonSelector.getDrawingEmoticon);

        this.drawingEmoticon$.subscribe((data: EmticonDraw[]) => {
            this.drawingEmoticonArr = data;
        });
    }

    ngOnInit() { }

    // check manually
    checkDrawEmoticonExist(row: number, col: number) {
        let title = '';
        let i = 0;

        for (i = 0; i < this.drawingEmoticonArr.length; i++) {
            if (this.drawingEmoticonArr[i].position.x === row && this.drawingEmoticonArr[i].position.y === col) {
                title = this.drawingEmoticonArr[i].item.title;
            }
        }

        return title;
    }
}