import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmoticonService } from './services/emoticon.service';

import { EmoticonEffect } from './store/effects/emoticon.effect';
import * as EmoticonReducer from './store/reducers/emoticon.reducer';
import { DrawingEmoticonComponent } from './drawing-emoticon/drawing.emticon.component';
import { DrawCopyEmoticonComponent } from './draw-copy-emoticon/draw-copy-emoticon.component';

@NgModule({
  declarations: [
    AppComponent,
    DrawingEmoticonComponent,
    DrawCopyEmoticonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({
      emoticons: EmoticonReducer.reducer
    }),
    EffectsModule.forRoot([
      EmoticonEffect
    ]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    EmoticonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
