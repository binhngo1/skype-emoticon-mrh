import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as EmoticonSelector from '../store/selectors/emoticon.selector';
import * as EmoticonAction from '../store/actions/emoticon.action';
import { Emoticon } from '../store/models/emoticon.model';
import { AppState } from '../store/models/app.state';
import { EmoticonState } from '../store/models/emoticon.state';
import { EmticonDraw } from '../store/models/emticon.draw';

@Component({
    selector: 'drawing-emoticon-component',
    templateUrl: './drawing.emticon.component.html',
    styleUrls: ['./drawing.emticon.component.css']
})

export class DrawingEmoticonComponent implements OnInit {
    selectedEmotion$: Observable<Emoticon>;
    drawingEmoticon$: Observable<EmticonDraw[]>;
    selectedEmotion: Emoticon = null;
    drawingEmoticonArr: EmticonDraw[] = [];
    statusMouse = null;

    rows = Array(5).fill(0).map((x,i)=>i);
    cols = Array(5).fill(0).map((x,i)=>i);

    constructor(
        private store: Store<AppState>,
    ) {
        this.selectedEmotion$ = this.store.select(EmoticonSelector.getSelectedEmoticon);
        this.drawingEmoticon$ = this.store.select(EmoticonSelector.getDrawingEmoticon);

        this.selectedEmotion$.subscribe((item: Emoticon) => {
            this.selectedEmotion = item;
        });

        this.drawingEmoticon$.subscribe((data: EmticonDraw[]) => {
            this.drawingEmoticonArr = data;
        });
    }

    ngOnInit() { 
        document.addEventListener('mouseup', () => {
            this.statusMouse = false;
        })
    }

    drawEmoticonToTable(row: number, col: number) {
        const selectedItem = new EmticonDraw();

        selectedItem.item = this.selectedEmotion;
        selectedItem.position = {
            x: row,
            y: col
        }

        this.store.dispatch(EmoticonAction.addEmoticonToDraw({selectedItem}));
    }

    // check manually
    checkDrawEmoticonExist(row: number, col: number) {

        let url = '';
        let i = 0;

        for (i = 0; i < this.drawingEmoticonArr.length; i++) {
            if (this.drawingEmoticonArr[i].position.x === row && this.drawingEmoticonArr[i].position.y === col) {
                url = this.drawingEmoticonArr[i].item.url;
            }
        }

        return url;
    }

    clearEmoticonDraw() {
        this.store.dispatch(EmoticonAction.clearEmoticonDraw());
    }

    mouseOverEmoticon(row: number, col: number ) {
        if (this.statusMouse) {
            if (this.selectedEmotion) {
                const selectedItem = new EmticonDraw();
    
                selectedItem.item = this.selectedEmotion;
                selectedItem.position = {
                    x: row,
                    y: col
                }
                this.store.dispatch(EmoticonAction.addEmoticonToDraw({selectedItem}));
                this.checkDrawEmoticonExist(row, col);
            }
        }
        // console.log(event);
    }
    mouseDown(row: number, col: number) {    
        this.statusMouse = true;
        this.drawEmoticonToTable(row, col);
    }

    // mouseUp(row: number, col: number) {
    //    this.statusMouse = false;
    // }

}