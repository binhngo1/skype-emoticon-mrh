import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Emoticon } from '../store/models/emoticon.model';

@Injectable()
export class EmoticonService {

    constructor(
       private httpClient: HttpClient
    ) { }

    getListEmoticon() {
        return this.httpClient.get<Emoticon[]>('/assets/listEmoticon.json');
    }
}