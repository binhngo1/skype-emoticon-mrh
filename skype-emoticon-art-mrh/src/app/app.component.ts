import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as EmoticonSelector from './store/selectors/emoticon.selector';
import * as EmoticonAction from './store/actions/emoticon.action';
import { Emoticon } from './store/models/emoticon.model';
import { AppState } from './store/models/app.state';
import { EmoticonState } from './store/models/emoticon.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  emoticon$: Observable<EmoticonState>;

  constructor(
    private store: Store<AppState>
  ) {
    this.emoticon$ = this.store.select(EmoticonSelector.getEmoticonState);
    this.store.dispatch(EmoticonAction.getListEmoticon());
  }

  selectItemToDraw(selectedItem: Emoticon) {
    this.store.dispatch(EmoticonAction.selectEmotionToDraw({selectedItem}));
  }


}
