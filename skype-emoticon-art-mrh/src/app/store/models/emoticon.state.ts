import { Emoticon } from './emoticon.model';
import { EmticonDraw } from './emticon.draw';

export class EmoticonState {
    items: Emoticon[];
    drawingItems: EmticonDraw[];
    selectedItem: Emoticon;

}