import { Emoticon } from "./emoticon.model";

export class EmticonDraw {
    item: Emoticon;
    position: {
        x: number;
        y: number;
    }
}