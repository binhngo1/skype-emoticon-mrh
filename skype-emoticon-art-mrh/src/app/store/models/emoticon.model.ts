export class Emoticon {
    id: number;
    url: string;
    title: string;
}