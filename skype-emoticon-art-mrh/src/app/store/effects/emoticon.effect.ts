import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { concatMap, map, catchError } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

import * as ActionEmoticon from '../actions/emoticon.action';
import { EmoticonService } from '../../services/emoticon.service';
import { Emoticon } from '../models/emoticon.model';


@Injectable()
export class EmoticonEffect {
    constructor(
        private actions$: Actions,
        private emoticonService: EmoticonService
    ) { }

    loadEmoticonList$ = createEffect(() => this.actions$.pipe(
        ofType(ActionEmoticon.getListEmoticon),
        concatMap(() =>
            this.emoticonService.getListEmoticon().pipe(
                map((newItemsArr: Emoticon[]) => ActionEmoticon.getListEmoticonSuccess({ newItemsArr })),
                catchError(() => EMPTY ))
            )
        )
    );


}