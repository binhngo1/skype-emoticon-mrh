import { Action, createReducer, on, State } from '@ngrx/store';

import * as EmoticonAction from '../actions/emoticon.action';
import { EmoticonState } from '../models/emoticon.state';
import { Emoticon } from '../models/emoticon.model';

export const initialState: EmoticonState = {
    items: [],
    drawingItems: [],       // Contain selected emoticon to draw
    selectedItem: null,     // Current selected emoticon
}

const featureReducer = createReducer(
    initialState,

    on(
        EmoticonAction.getListEmoticonSuccess,
        (state, { newItemsArr }) => ({ ...state, items: [...state.items, ...newItemsArr] })
    ),

    on(
        EmoticonAction.selectEmotionToDraw,
        (state, { selectedItem }) => ({ ...state, selectedItem })
    ),

    on(
        EmoticonAction.addEmoticonToDraw,
        (state, { selectedItem }) => ({...state, drawingItems: [...state.drawingItems, selectedItem]})
    ),
    on(
        EmoticonAction.clearEmoticonDraw,
        (state) => ({...state, drawingItems: []})
    )
);

export function reducer(state: EmoticonState | undefined, action: Action) {
    return featureReducer(state, action);
}