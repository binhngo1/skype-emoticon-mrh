import { createAction, props } from '@ngrx/store';
import { Emoticon } from '../models/emoticon.model';
import { EmticonDraw } from '../models/emticon.draw';

export const getListEmoticon = createAction('[Emoticon] get list emoticon');

export const getListEmoticonSuccess = createAction(
    '[Emoticon] get list emoticon success', 
    props<{ newItemsArr: Emoticon[] }>()
);

export const selectEmotionToDraw = createAction(
    '[Emoticon] select emoticon to draw', 
    props<{ selectedItem: Emoticon }>()
);

export const addEmoticonToDraw = createAction(
    '[Emoticon] add emoticon to draw',
    props<{ selectedItem: EmticonDraw}>()
);

export const clearEmoticonDraw = createAction(
    '[Emoticon] clear emoticon draw'
);