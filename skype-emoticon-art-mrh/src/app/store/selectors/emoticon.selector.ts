import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EmoticonState } from '../models/emoticon.state';

export const getEmoticonState = createFeatureSelector<EmoticonState>('emoticons');

// export const getEmoticonConpletedItems = createSelector(
//     getEmoticonState,
//     (emoticon) => (emoticon.items)
// );
export const getSelectedEmoticon = createSelector(
    getEmoticonState,
    (state) => (state.selectedItem)
);

export const getDrawingEmoticon = createSelector(
    getEmoticonState,
    (state) => (state.drawingItems)
);